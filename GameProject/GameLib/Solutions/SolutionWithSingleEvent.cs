﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLib.Events;
using GameLib.EventsStore;

namespace GameLib.Solutions
{
    public class SolutionWithSingleEvent: ISolution
    {
        public string Text { get; set; }
        public List<PairOfCharacteristicAndValue> Changes;

        [NonSerialized]
        public Action<List<PairOfCharacteristicAndValue>> HandlerSolution;
        [NonSerialized]
        public Action<long, int> HandlerResultEvent;

        public StorageOfCleanedEvent ResultEvent;

        public IEnumerable<string> GetSolutionChanges()
        {
            return Changes.Select(change => change.ToString());
        }

        public void HandleSolution()
        {
            HandlerResultEvent?.Invoke(ResultEvent.Id,ResultEvent.WaitingTime);
            HandlerSolution?.Invoke(Changes);
        }
    }
}