﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLib.Events;

namespace GameLib.Solutions
{
    public class SolutionWithInstantChanges: ISolution
    {
        public string Text { get; set; }
        public List<PairOfCharacteristicAndValue> Changes;

        [NonSerialized]
        public Action<List<PairOfCharacteristicAndValue>> HandlerSolution;

        public IEnumerable<string> GetSolutionChanges()
        {
            return Changes.Select(change=>change.ToString());
        }

        public void HandleSolution()
        {
            HandlerSolution?.Invoke(Changes);
        }
    }
}