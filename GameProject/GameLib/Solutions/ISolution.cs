﻿using System.Collections.Generic;
using System.Linq;
using GameLib.Events;

namespace GameLib.Solutions
{
    public interface ISolution
    {
       string Text { get; set; }
       IEnumerable<string> GetSolutionChanges();
       void HandleSolution();
    }
}