﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.EventFactories;
using GameLib.Events;
using GameLib.ForSaveAndLoad;
using GameLib.Exceptions;

namespace GameLib.EventsStore
{
    public class EventsStores
    {
        private readonly Dictionary<long,IEvent> _events;
        private readonly InstantEventsStore _instantEvents;
        private readonly DeferredEventsStore _deferredEvents;
        private readonly EndEventsStore _endEventsStore;
        private readonly Func<long, IEvent> _getEventById;

        private EventsStores(Dictionary<long, IEvent> events)
        {
            _getEventById += GetEventById;
            _events = events;
        }

        public EventsStores(Dictionary<long, IEvent> events,EventsStoresMemento memento ) : this(events)
        {
            _instantEvents = new InstantEventsStore(memento.InstantEvents, _getEventById);
            _deferredEvents = new DeferredEventsStore(memento.DeferredEvents, _getEventById);
            _endEventsStore = new EndEventsStore(memento.EndEvents, _getEventById);
        }

        public EventsStores(Dictionary<long, IEvent> events, DefaulteEventsStoreMemento defaulteMemento):this(events)
        {
            _instantEvents = new InstantEventsStore(defaulteMemento.InstantEvents, _getEventById);
            _deferredEvents = new DeferredEventsStore(defaulteMemento.DeferredEvents, _getEventById);
            _endEventsStore = new EndEventsStore(defaulteMemento.EndEvents, _getEventById);
        }

        public EventsStoresMemento CreateMemento()
        {
            return new EventsStoresMemento(_instantEvents.CreateMemento(), _deferredEvents.CreateMemento(), _endEventsStore.CreateMemento());
        }

        public IEvent GetEvent(int nowTime, IEnumerable<PairOfCharacteristicAndValue> characteristic)
        {
            var factory = CreateEventFactory(nowTime, characteristic);
            return factory.GetEvent();
        }

        public void AddResultToExpect(long id, int waitingTime)
        {
            _deferredEvents.AddResultToExpect(id,waitingTime);
        }

        private EventFactory CreateEventFactory(int nowTime, IEnumerable<PairOfCharacteristicAndValue> characteristic)
        {
            var endEventFactory = new EndEventFactory(_endEventsStore, characteristic);
            var deferredEventFactory = new DeferredEventFactory(_deferredEvents, nowTime);
            var instantEventFactory = new InstantEventFactory(_instantEvents);
            endEventFactory.SetNextFactory(deferredEventFactory).SetNextFactory(instantEventFactory);
            return endEventFactory;
        }

        public IEvent GetEventById(long id)
        {
            if (!_events.ContainsKey(id))
                throw new IndexByEventNotFound($"Event in Id = {id} not found");

            return _events[id] ;
        }
    }
}