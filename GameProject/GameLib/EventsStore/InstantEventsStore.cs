﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.ForSaveAndLoad;
using Newtonsoft.Json;
using GameLib.Exceptions;

namespace GameLib.EventsStore
{
    public class InstantEventsStore
    {
        private List<long> _instantEvents;
        private Basket _basket = new Basket();
        private Func<long, IEvent> _getEventById;

        private InstantEventsStore(Func<long, IEvent> getEventById)
        {
            _getEventById = getEventById;
        }

        public InstantEventsStore(List<long> instantEvents, Func<long, IEvent> getEventById) : this(getEventById)
        {
            if (instantEvents.Count == 0)
                throw new ArgumentException();

            _instantEvents = instantEvents;
        }

        public InstantEventsStore(InstantEventsMemento memento, Func<long, IEvent> getEventById) : this(getEventById)
        {
            _instantEvents = memento.InstantEvents;
            _basket = new Basket(memento.Basket);
        }

        public InstantEventsMemento CreateMemento()
        {
            return new InstantEventsMemento(_instantEvents, _basket.BasketList);
        }

        public bool IsTimeForInstantEvent()
        {
            return _instantEvents.Count != 0 || _basket.BasketList.Count != 0;
        }

        public IEvent GetEvent()
        {
            if (!IsTimeForInstantEvent())
                throw new NotEventsExeption("Instant events store is empty");

            ReturnEventsFromBasket();
            var index = new Random(DateTime.Now.Millisecond).Next(_instantEvents.Count - 1);
            var id = _instantEvents[index];
            var newEvent = GetEventById(id) as InstantEvent;
            SendEventToBasket(newEvent.Id, newEvent.TimeToRollback);
            return newEvent;
        }

        private IEvent GetEventById(long id)
        {
            try
            {
                return _getEventById.Invoke(id) ;
            }

            catch (IndexByEventNotFound)
            {
                _instantEvents.Remove(id);
                return GetEvent();
            }
        }

        private void SendEventToBasket(long id, int timeToRollback)
        {
            _instantEvents.Remove(id);
            _basket.AddEvent(id, timeToRollback);
        }

        private void ReturnEventsFromBasket()
        {
            do
            {
                foreach (var @event in _basket.ReturnEventsFromBasket())
                {
                    _instantEvents.Add(@event);
                }
            } while (_instantEvents.Count == 0);
        }
    }
}