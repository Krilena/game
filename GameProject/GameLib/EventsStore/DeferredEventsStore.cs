﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.ForSaveAndLoad;

namespace GameLib.EventsStore
{
    public class DeferredEventsStore
    {
        private readonly List<long> _deferredEvents;
        private readonly Basket _basket = new Basket();
        private readonly int _intervalTimeBetweenEvents = 10;
        private StorageOfCleanedEvent _expectedResult;
        private int _countdownTime;
        private Func<long, IEvent> _getEventById;

        private DeferredEventsStore(Func<long, IEvent> getEventById)
        {
            _getEventById = getEventById;
        }

        public DeferredEventsStore(List<long> deferredEvents, Func<long, IEvent> getEventById): this(getEventById)
        {
            if (deferredEvents.Count == 0)
                throw new ArgumentException();

            _deferredEvents = deferredEvents;
        }

        public DeferredEventsStore(DeferredEventsMemento memento, Func<long, IEvent> getEventById): this(getEventById)
        {
            _deferredEvents = memento.DeferredEvents;
            _basket = new Basket( memento.Basket);
            _expectedResult = memento.ExpectedResult;
            _countdownTime = memento.CountdownTime;
            _intervalTimeBetweenEvents = memento.IntervalTimeBetweenEvents;
        }
        public DeferredEventsMemento CreateMemento()
        {
            return new DeferredEventsMemento(_deferredEvents,_basket.BasketList,_expectedResult,_intervalTimeBetweenEvents,_countdownTime);
        }

        public IEvent GetEvent(int nowTime)
        {
            if (!IsTimeForResultEvent())
                return GetNewDeferredEvent();

            var result = GetResultEvent(nowTime);
            _countdownTime = nowTime;
            return result;
        }

        public bool IsTimeForDeferredEvent(int nowTime)
        {
            if (_deferredEvents.Count == 0 && _basket.BasketList.Count == 0)
                return false;

            return IsTimeForResultEvent() || IsTimeForNewDeferredEvent(nowTime);
        }

        public void AddResultToExpect(long id, int waitingTime)
        {
            _expectedResult = new StorageOfCleanedEvent { Id = id, WaitingTime = waitingTime };
        }

        private DeferredEvent GetNewDeferredEvent()
        {
            ReturnEventsFromBasket();
            var index = new Random(DateTime.Now.Millisecond).Next(_deferredEvents.Count - 1);
            var id = _deferredEvents[index];
            var newEvent = _getEventById.Invoke(id) as DeferredEvent;
            SendEventToRollback(newEvent.Id,newEvent.TimeToRollback );;

            return newEvent;
        }

        private ResultEvent GetResultEvent(int nowTime)
        {
            var resultEvent = _getEventById(_expectedResult.Id) as ResultEvent;
            _expectedResult = null;
            _countdownTime = nowTime;
            return resultEvent;
        }

        private bool IsTimeForResultEvent()
        {
            if (_expectedResult == null)
                return false;

            if (_expectedResult.WaitingTime <= 0)
                return true;

            _expectedResult.WaitingTime--;
            return false;
        }

        private bool IsTimeForNewDeferredEvent(int nowTime)
        {
            if (_expectedResult != null)
                return false;

            var pastTime = nowTime - _countdownTime;
            if (pastTime <= _intervalTimeBetweenEvents)
                return false;

            var probability = (new Random()).Next(5);

            return (pastTime - _intervalTimeBetweenEvents) >= probability;
        }

        private void SendEventToRollback(long id, int timeToRollback)
        {
            _deferredEvents.Remove(id);
            _basket.AddEvent(id, timeToRollback);
        }

        private void ReturnEventsFromBasket()
        {
            do
            {
                foreach (var id in _basket.ReturnEventsFromBasket())
                {
                    _deferredEvents.Add(id);
                }
            } while (_deferredEvents.Count == 0);
        }
    }
}