﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.ForSaveAndLoad;

namespace GameLib.EventsStore
{
    public class EndEventsStore
    {
        private List<long> _endEvents;
        private Func<long, IEvent> _getEventById;

        private EndEventsStore(Func<long, IEvent> getEventById)
        {
            _getEventById = getEventById;
        }

        public EndEventsStore(EndEventsMemento memento, Func<long, IEvent> getEventById) :this(getEventById)
        {
            _endEvents = memento.EndEvents;
        }

        public EndEventsStore(List<long> endEvents, Func<long, IEvent> getEventById) : this(getEventById)
        {
            _endEvents = endEvents;
        }

        public EndEventsMemento CreateMemento()
        {
            return new EndEventsMemento(_endEvents);
        }

        public bool IsTimeForEndEvent(IEnumerable<PairOfCharacteristicAndValue> nowCharacteristics)
        {
            if (_endEvents.Count == 0)
                return false;

            foreach (var id in _endEvents)
            {
                var endEvent = _getEventById.Invoke(id)as EndEvent;
                if (endEvent.NecessaryConditions.IsSatisfiesСonditions(nowCharacteristics))
                    return true;
            }

            return false;
        }

        public IEvent GetEvent(IEnumerable<PairOfCharacteristicAndValue> nowCharacteristics)
        {
            var possibleEndings = new List<EndEvent>();
            foreach (var id in _endEvents)
            {
                var endEvent = _getEventById.Invoke(id) as EndEvent;
                if (endEvent.NecessaryConditions.IsSatisfiesСonditions(nowCharacteristics))
                    possibleEndings.Add(endEvent);
            }

            return GetNewEvent(possibleEndings);
        }

        private IEvent GetNewEvent(List<EndEvent> endEvents)
        {
            var index = new Random(DateTime.Now.Millisecond).Next(endEvents.Count);
            return endEvents[index];
        }
    }
}