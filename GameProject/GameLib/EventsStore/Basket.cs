﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.ForSaveAndLoad;
using Newtonsoft.Json;

namespace GameLib.EventsStore
{ 
    public class Basket
    {
        public List<StorageOfCleanedEvent> BasketList { get; private set; } = new List<StorageOfCleanedEvent>();

        public Basket() { }

        public Basket(List<StorageOfCleanedEvent> basketList)
        {
            BasketList = basketList;
        }

        public void AddEvent(long id, int timeToReturn)
        {
            var eventRollback = new StorageOfCleanedEvent { Id = id, WaitingTime = timeToReturn };
            BasketList.Add(eventRollback);
        }

        public List<long> ReturnEventsFromBasket()
        {
            DecreaseTimeToRollbackEventsInBasket();
            var returnEvents = new List<long>();

            for (var i = 0; i < BasketList.Count; i++)
            {
                if (BasketList[i].WaitingTime > 0)
                    continue;

                returnEvents.Add(BasketList[i].Id);
                BasketList.RemoveAt(i);
                i--;
            }

            return returnEvents;
        }

        private void DecreaseTimeToRollbackEventsInBasket()
        {
            foreach (var eventRollback in BasketList)
                eventRollback.WaitingTime--;
        }
    }
}