﻿using GameLib.Events;

namespace GameLib.EventFactories
{
    public abstract class EventFactory
    {
        private EventFactory _nextFactory;

        public EventFactory SetNextFactory(EventFactory nextFactory)
        {
            _nextFactory = nextFactory;
            return nextFactory;
        }

        public virtual IEvent GetEvent()
        {
            return _nextFactory?.GetEvent();
        }
    }
}