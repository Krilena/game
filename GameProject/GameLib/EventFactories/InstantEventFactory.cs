﻿using GameLib.Events;
using GameLib.EventsStore;
using GameLib.Exceptions;

namespace GameLib.EventFactories
{
    public class InstantEventFactory: EventFactory
    {
        private InstantEventsStore _instantEventsStore;

        public InstantEventFactory(InstantEventsStore instantEventsStore)
        {
            _instantEventsStore = instantEventsStore;
        }

        public override IEvent GetEvent()
        {
            if (!_instantEventsStore.IsTimeForInstantEvent())
                throw new NotEventsExeption("Instant event store is empty");
            
            return _instantEventsStore.GetEvent();
        }
    }
}