﻿using System.Collections.Generic;
using GameLib.Events;
using GameLib.EventsStore;

namespace GameLib.EventFactories
{
    public class EndEventFactory: EventFactory
    {
        private EndEventsStore _endEventsStore;
        private IEnumerable<PairOfCharacteristicAndValue> _characteristics;
        public EndEventFactory(EndEventsStore endEventsStore, IEnumerable<PairOfCharacteristicAndValue> characteristics)
        {
            _endEventsStore = endEventsStore;
            _characteristics = characteristics;
        }

        public override IEvent GetEvent()
        {
            if (!_endEventsStore.IsTimeForEndEvent(_characteristics))
                return base.GetEvent();

            return _endEventsStore.GetEvent(_characteristics);
        }
    }
}