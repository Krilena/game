﻿using System.Collections.Generic;
using GameLib.Events;
using GameLib.EventsStore;

namespace GameLib.EventFactories
{
    public class DeferredEventFactory: EventFactory
    {
        private DeferredEventsStore _deferredEventsStore;
        private int _nowTime;

        public DeferredEventFactory(DeferredEventsStore deferredEventsStore, int nowTime)
        {
            _deferredEventsStore = deferredEventsStore;
            _nowTime = nowTime;
        }

        public override IEvent GetEvent()
        {
            if (!_deferredEventsStore.IsTimeForDeferredEvent(_nowTime))
                return base.GetEvent();

            return _deferredEventsStore.GetEvent(_nowTime);
        }
    }
}