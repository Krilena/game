﻿using System.Collections.Generic;
using System.Linq;
using GameLib.Events;
using GameLib.EventsStore;
using GameLib.Solutions;
using System;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.Serialization;
using GameLib.ForSaveAndLoad;

namespace GameLib
{
    public class Game
    {
        private EventsStores _eventsStores;
        private User _user;
        private int _time;
        private CurrentEvent _currentEvent;
        private IGameLoader _gameLoader;
        private IGameSaver _gameSaver;

        public Game(IGameLoader gameLoader, IGameSaver gameSaver)
        {
            _gameLoader = gameLoader;
            _gameSaver = gameSaver;
        }

        public void LoadNewGame()
        {
            var loadedData = _gameLoader.LoadNewGame();
            _eventsStores = new EventsStores(loadedData.Item1, loadedData.Item2);
            _user = new User();
            _currentEvent = new CurrentEvent();
            AddNeedMethodsToSolutionHandlers(loadedData.Item1);
            _time = 1;
        }

        public void LoadSavedGame()
        {
            try
            {
                var loadedData = _gameLoader.LoadSavedGame();
                _eventsStores = new EventsStores(loadedData.Item1, loadedData.Item2.EventsStores);
                _user = new User(loadedData.Item2.Characteristics);
                _currentEvent = new CurrentEvent {Event = _eventsStores.GetEventById(loadedData.Item2.IdCurrentEvent)};
                AddNeedMethodsToSolutionHandlers(loadedData.Item1);
                _time = loadedData.Item2.Time;
            }
            catch
            {
                LoadNewGame();
            }
        }

        public IEvent GetEvent()
        {
            IEvent newEvent;
            
            if (!_currentEvent.IsLoad && _currentEvent.Event != null)
                newEvent = _currentEvent.Event;
            else
                newEvent = _eventsStores.GetEvent(_time, _user.GetCharacteristic());

            _currentEvent = new CurrentEvent {Event = newEvent, IsLoad = true};
            _time++;
            SaveGame();
            return newEvent;
        }

        public IEnumerable<PairOfCharacteristicAndValue> GetCharacteristic()
        {
            return _user.GetCharacteristic();
        }

        private void SaveGame()
        {
            var memento = new GameMemento(_user.GetCharacteristic(), _time, _eventsStores.CreateMemento(),_currentEvent.Event);
            _gameSaver.Save(memento);
        }

        private void AddNeedMethodsToSolutionHandlers(Dictionary<long, IEvent> events)
        {
            foreach (var @event in events.Values)
                foreach (var solution in @event.GetSolutions())
                    switch (solution)
                    {
                        case SolutionWithInstantChanges solutionWithInstantChanges:
                            solutionWithInstantChanges.HandlerSolution += _user.ChangeСharacteristics;
                            break;
                        case SolutionWithSingleEvent solutionWithSingleEvent:
                            solutionWithSingleEvent.HandlerSolution += _user.ChangeСharacteristics;
                            solutionWithSingleEvent.HandlerResultEvent += _eventsStores.AddResultToExpect;
                            break;
                    }
        }
    }
}