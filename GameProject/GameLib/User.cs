﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.ForSaveAndLoad;

namespace GameLib
{
    public class User
    {
        public  Dictionary<Characteristic, int> _characteristics { get; private set; }

        public User()
        {
            _characteristics = new Dictionary<Characteristic, int>
            {
                {Characteristic.Mana, 50},
                {Characteristic.People, 50},
                {Characteristic.Faith, 50},
                {Characteristic.Army, 50}
            };
        }

        public User(IEnumerable<PairOfCharacteristicAndValue> characteristics) : this()
        {
            foreach (var characteristic in characteristics)
            {
                _characteristics[characteristic.Characteristic] = characteristic.Count;
            }
        }

        public IEnumerable<PairOfCharacteristicAndValue> GetCharacteristic()
        {
            return _characteristics.Select(characteristic 
                => new PairOfCharacteristicAndValue {Characteristic = characteristic.Key, Count = characteristic.Value});
        }

        public void ChangeСharacteristics(IEnumerable<PairOfCharacteristicAndValue> changes)
        {
            foreach (var change in changes)
            {
                _characteristics[change.Characteristic] += change.Count;
            }
        }
    }
}
