﻿using System.Collections.Generic;
using GameLib.Events;

namespace GameLib
{
    public class ConditionsForStartOfEndEvent
    {
        public List<PairOfCharacteristicAndValue> Conditions;

        public bool IsSatisfiesСonditions(IEnumerable<PairOfCharacteristicAndValue> characteristics)
        {
            foreach (var characteristic in characteristics)
            {
                foreach (var condition in Conditions)
                {
                    if (characteristic.Characteristic == condition.Characteristic &&
                        IsSatisfiesСondition(characteristic.Count, condition.Count))
                        return true;
                }
            }

            return false;
        }

        private bool IsSatisfiesСondition(int characteristicCount, int conditionCount)
        {
            if (conditionCount == 0 & characteristicCount <= conditionCount)
                return true;
            if (conditionCount == 100 && characteristicCount >= conditionCount)
                return true;
            return false;
        }
    }
}