﻿using System.Collections.Generic;
using GameLib.Solutions;

namespace GameLib.Events
{
    public class EndEvent: IEvent
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public string FileName { get; set; }
        public ConditionsForStartOfEndEvent NecessaryConditions { get; set; }
        public IEnumerable<ISolution> GetSolutions()
        {
            return new List<ISolution>();
        }
    }
}