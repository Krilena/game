﻿using System.Collections.Generic;
using GameLib.Solutions;

namespace GameLib.Events
{
    public class ResultEvent: IEvent
    {
        public long Id;
        public string Text { get; set; }
        public string FileName { get; set; }
        public int WaitingTime { get; set; }
        public SolutionWithInstantChanges Solution { get; set; }
        public IEnumerable<ISolution> GetSolutions()
        {
            return new List<ISolution>{Solution};
        }
    }
}