﻿using System;
using System.Collections.Generic;
using GameLib.Solutions;

namespace GameLib.Events
{    
    public interface IEvent
    {
        string Text { get; set; }
        string FileName { get; set; }
        IEnumerable<ISolution> GetSolutions();
    }
}