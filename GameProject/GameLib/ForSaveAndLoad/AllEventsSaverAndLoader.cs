﻿using System.Collections.Generic;
using System.IO;
using GameLib.Events;
using Newtonsoft.Json;

namespace GameLib.ForSaveAndLoad
{
    public class AllEventsSaverAndLoader
    {
        private const string InstantEvents = "[InstantEvents]";
        private const string DeferredEvents = "[DeferredEvents]";
        private const string EndEvents = "[EndEvents]";
        private const string ResultEvents = "[ResultEvents]";

        private string _fileName;

        public AllEventsSaverAndLoader(string fileName)
        {
            _fileName = fileName;
        }

        public Dictionary<long, IEvent> Load()
        {
            var events = new Dictionary<long, IEvent>();

            using (var sr = new StreamReader(_fileName))
            {
                var line = sr.ReadLine();
                while (line!=null)
                {
                    switch (line)
                    {
                        case InstantEvents:
                            var instantEvent = JsonConvert.DeserializeObject<InstantEvent>(sr.ReadLine());
                            if (!events.ContainsKey(instantEvent.Id))
                                events.Add(instantEvent.Id, instantEvent);
                            break;
                        case DeferredEvents:
                            var deferredEvent = JsonConvert.DeserializeObject<DeferredEvent>(sr.ReadLine());
                            if (!events.ContainsKey(deferredEvent.Id))
                                events.Add(deferredEvent.Id, deferredEvent);
                            break;
                        case EndEvents:
                            var endEvent = JsonConvert.DeserializeObject<EndEvent>(sr.ReadLine());
                            if (!events.ContainsKey(endEvent.Id))
                                events.Add(endEvent.Id, endEvent);
                            break;
                        case ResultEvents:
                            var resultEvent = JsonConvert.DeserializeObject<ResultEvent>(sr.ReadLine());
                            if (!events.ContainsKey(resultEvent.Id))
                                events.Add(resultEvent.Id, resultEvent);
                            break;
                    }

                    line = sr.ReadLine();
                }
            }

            return events;
        }

        public void Save(Dictionary<long, IEvent> events)
        {
            using (var sr = new StreamWriter(_fileName))
            {
                foreach (var @event in events.Values)
                {
                    switch (@event)
                    {
                        case InstantEvent instantEvent:
                            sr.WriteLine(InstantEvents);
                            sr.WriteLine(JsonConvert.SerializeObject(instantEvent));
                            break;
                        case DeferredEvent deferredEvent:
                            sr.WriteLine(DeferredEvents);
                            sr.WriteLine(JsonConvert.SerializeObject(deferredEvent));
                            break;
                        case EndEvent endEvent:
                            sr.WriteLine(EndEvents);
                            sr.WriteLine(JsonConvert.SerializeObject(endEvent));
                            break;
                        case ResultEvent resultEvent:
                            sr.WriteLine(ResultEvents);
                            sr.WriteLine(JsonConvert.SerializeObject(resultEvent));
                            break;
                    }
                }
            }
        }
    }
}