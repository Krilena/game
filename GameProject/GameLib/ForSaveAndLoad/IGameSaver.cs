﻿using System.Collections.Generic;

namespace GameLib.ForSaveAndLoad
{
    public interface IGameSaver
    {
        void Save(GameMemento memento);
    }
}