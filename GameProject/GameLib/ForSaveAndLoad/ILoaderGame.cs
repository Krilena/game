﻿using System.Collections.Generic;
using GameLib.Events;

namespace GameLib.ForSaveAndLoad
{
    public interface IGameLoader
    {
        (Dictionary<long, IEvent>, DefaulteEventsStoreMemento) LoadNewGame();
        (Dictionary<long, IEvent>, GameMemento) LoadSavedGame();
    }
}