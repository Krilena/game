﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using GameLib.Events;
using GameLib.EventsStore;
using Newtonsoft.Json;

namespace GameLib.ForSaveAndLoad
{
    public class GameLoader : IGameLoader
    {
        private string _defaultFileName;
        private string _saveFileName;

        public GameLoader(string defaultFileName, string saveFileName)
        {
            _defaultFileName = defaultFileName;
            _saveFileName = saveFileName;
        }

        public (Dictionary<long, IEvent>, DefaulteEventsStoreMemento) LoadNewGame()
        {
            var events = new AllEventsSaverAndLoader(_defaultFileName).Load();
            var defaulteMemento = CreateDefaulteEvents(events);

            return (events, defaulteMemento);
        }

        public (Dictionary<long, IEvent>, GameMemento) LoadSavedGame()
        {
            var events = new AllEventsSaverAndLoader(_defaultFileName).Load();
            var gameMemento =  JsonConvert.DeserializeObject<GameMemento>(File.ReadAllText(_saveFileName));

            return (events, gameMemento);
        }

        private DefaulteEventsStoreMemento CreateDefaulteEvents(Dictionary<long, IEvent> events)
        {
            var instantEvents = new List<long>();
            var deferredEvents = new List<long>();
            var endEvents = new List<long>();

            foreach (var @event in events.Values)
            {
                switch (@event)
                {
                    case InstantEvent instantEvent:
                        instantEvents.Add(instantEvent.Id);
                        break;
                    case DeferredEvent deferredEvent:
                        deferredEvents.Add(deferredEvent.Id);
                        break;
                    case EndEvent endEvent:
                        endEvents.Add(endEvent.Id);
                        break;
                }
            }

            return new DefaulteEventsStoreMemento(instantEvents, deferredEvents, endEvents);
        }
    }
}