﻿using System.IO;
using Newtonsoft.Json;

namespace GameLib.ForSaveAndLoad
{
    public class GameSaver: IGameSaver
    {
        private string _fileName;
        public GameSaver(string fileName)
        {
            _fileName = fileName;
        }

        public void Save(GameMemento memento)
        {
            var json = JsonConvert.SerializeObject(memento);
            
            File.WriteAllText(_fileName,json);
        }
    }
}