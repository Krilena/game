﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.EventsStore;

namespace GameLib.ForSaveAndLoad
{
    [Serializable]
    public class InstantEventsMemento: ISerializable
    {
        public List<long> InstantEvents { get; private set; } 
        public List<StorageOfCleanedEvent> Basket { get; private set; }

        public InstantEventsMemento(List<long> instantEvents, List<StorageOfCleanedEvent> basket)
        {
            InstantEvents = instantEvents;
            Basket = basket;
        }

        public InstantEventsMemento(SerializationInfo info, StreamingContext context)
        {
            InstantEvents = (List<long>)info.GetValue("InstantEvents", typeof(List<long>));
            Basket = (List<StorageOfCleanedEvent>)info.GetValue("Basket", typeof(List<StorageOfCleanedEvent>));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("InstantEvents", InstantEvents);
            info.AddValue("Basket",Basket);
        }
    }
}