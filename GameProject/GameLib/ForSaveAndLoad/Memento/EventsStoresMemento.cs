﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.EventsStore;

namespace GameLib.ForSaveAndLoad
{
    [Serializable]
    public class EventsStoresMemento: ISerializable
    {
        public InstantEventsMemento InstantEvents { get; private set; }
        public DeferredEventsMemento DeferredEvents { get; private set; }
        public EndEventsMemento EndEvents { get; private set; }

        public EventsStoresMemento(InstantEventsMemento instantEvents, DeferredEventsMemento deferredEvents, EndEventsMemento endEvents)
        {
            InstantEvents = instantEvents;
            DeferredEvents = deferredEvents;
            EndEvents = endEvents;
        }

        public EventsStoresMemento(SerializationInfo info, StreamingContext context)
        {
            InstantEvents = (InstantEventsMemento)info.GetValue("InstantEvents", typeof(InstantEventsMemento));
            DeferredEvents = (DeferredEventsMemento)info.GetValue("DeferredEvents", typeof(DeferredEventsMemento));
            EndEvents = (EndEventsMemento)info.GetValue("EndEvents", typeof(EndEventsMemento));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("InstantEvents", InstantEvents);
            info.AddValue("DeferredEvents", DeferredEvents);
            info.AddValue("EndEvents", EndEvents);
        }
    }
}