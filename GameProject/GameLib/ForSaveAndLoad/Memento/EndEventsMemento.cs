﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.EventsStore;

namespace GameLib.ForSaveAndLoad
{
    [Serializable]
    public class EndEventsMemento: ISerializable
    {
        public List<long> EndEvents { get; private set; }

        public EndEventsMemento(List<long> endEvents)
        {
            EndEvents = endEvents;
        }

        public EndEventsMemento(SerializationInfo info, StreamingContext context)
        {
            EndEvents = (List<long>)info.GetValue("EndEvents", typeof(List<long>));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("EndEvents", EndEvents);
        }
    }
}