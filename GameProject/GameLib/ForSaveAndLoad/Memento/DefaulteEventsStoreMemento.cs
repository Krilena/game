﻿using System.Collections.Generic;

namespace GameLib.ForSaveAndLoad
{
    public class DefaulteEventsStoreMemento
    {
        public List<long> InstantEvents { get; private set; }
        public List<long> DeferredEvents { get; private set; }
        public List<long> EndEvents { get; private set; }

        public DefaulteEventsStoreMemento(List<long> instantEvents, List<long> deferredEvents, List<long> endEvents)
        {
            InstantEvents = instantEvents;
            DeferredEvents = deferredEvents;
            EndEvents = endEvents;
        }
    }
}