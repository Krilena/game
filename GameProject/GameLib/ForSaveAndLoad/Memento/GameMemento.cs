﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.Events;
using GameLib.EventsStore;

namespace GameLib.ForSaveAndLoad
{
    [Serializable]
    public class GameMemento: ISerializable
    {
        public List<PairOfCharacteristicAndValue> Characteristics { get; private set; }
        public int Time { get; private set; }
        public EventsStoresMemento EventsStores { get; private set; }
        public long IdCurrentEvent { get; private set; }

        public GameMemento(IEnumerable<PairOfCharacteristicAndValue> characteristics,int time,EventsStoresMemento eventsStores, IEvent currentEvent)
        {
            Characteristics = new List<PairOfCharacteristicAndValue>( characteristics);
            Time = time;
            EventsStores = eventsStores;
            IdCurrentEvent = GetEventId(currentEvent);
        }

        public GameMemento(SerializationInfo info, StreamingContext context)
        {
            Characteristics = (List<PairOfCharacteristicAndValue>)info.GetValue("Characteristics", typeof(List<PairOfCharacteristicAndValue>));
            Time = (int)info.GetValue("Time", typeof(int));
            EventsStores = (EventsStoresMemento)info.GetValue("EventsStores", typeof(EventsStoresMemento));
            IdCurrentEvent = (long) info.GetValue("IdCurrentEvent", typeof(long));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Characteristics", Characteristics);
            info.AddValue("Time", Time);
            info.AddValue("EventsStores", EventsStores);
            info.AddValue("IdCurrentEvent",IdCurrentEvent);
        }

        private long GetEventId(IEvent @event)
        {
            switch (@event)
            {
                case InstantEvent instantEvent:
                    return instantEvent.Id;
                case DeferredEvent deferredEvent:
                    return deferredEvent.Id;
                case ResultEvent resultEvent:
                    return resultEvent.Id;
                case EndEvent endEvent:
                    return endEvent.Id;
                default:
                    return 0;
            }
        }
    }
}