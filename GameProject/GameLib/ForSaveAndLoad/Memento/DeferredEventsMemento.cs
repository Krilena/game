﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GameLib.EventsStore;

namespace GameLib.ForSaveAndLoad
{
    [Serializable]
    public class DeferredEventsMemento: ISerializable
    {
        public  List<long> DeferredEvents { get; private set; }
        public  List<StorageOfCleanedEvent> Basket { get; private set; }
        public StorageOfCleanedEvent ExpectedResult { get; private set; }
        public  int IntervalTimeBetweenEvents { get; private set; }
        public int CountdownTime { get; private set; }

        public DeferredEventsMemento(List<long> deferredEvents, List<StorageOfCleanedEvent> basket, StorageOfCleanedEvent expectedResult, int intervalTime, int countdownTime)
        {
            DeferredEvents = deferredEvents;
            Basket = basket;
            ExpectedResult = expectedResult;
            IntervalTimeBetweenEvents = intervalTime;
            CountdownTime = countdownTime;
        }

        public DeferredEventsMemento(SerializationInfo info, StreamingContext context)
        {
            DeferredEvents = (List<long>)info.GetValue("DeferredEvents", typeof(List<long>));
            Basket = (List<StorageOfCleanedEvent>)info.GetValue("Basket", typeof(List<StorageOfCleanedEvent>));
            ExpectedResult = (StorageOfCleanedEvent) info.GetValue("ExpectedResult", typeof(StorageOfCleanedEvent));
            IntervalTimeBetweenEvents = (int) info.GetValue("IntervalTimeBetweenEvents", typeof(int));
            CountdownTime = (int)info.GetValue("CountdownTime", typeof(int));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("DeferredEvents", DeferredEvents);
            info.AddValue("Basket", Basket);
            info.AddValue("ExpectedResult", ExpectedResult);
            info.AddValue("IntervalTimeBetweenEvents", IntervalTimeBetweenEvents);
            info.AddValue("CountdownTime", CountdownTime);
        }
    }
}