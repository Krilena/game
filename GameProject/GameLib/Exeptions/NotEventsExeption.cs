﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLib.Exceptions
{
    class NotEventsExeption: Exception
    {
        public readonly string Message;

        public NotEventsExeption(string message)
        {
            Message = message;
        }
    }
}
