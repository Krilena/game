﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLib.Exceptions
{
    class IndexByEventNotFound: Exception
    {
        public readonly string Message;

        public IndexByEventNotFound(string message)
        {
            Message = message;
        }
    }
}
