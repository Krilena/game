﻿using System;

namespace GameLib
{
    [Serializable]
    public enum Characteristic
    {
        Mana,
        Faith,
        Army,
        People
    }
}