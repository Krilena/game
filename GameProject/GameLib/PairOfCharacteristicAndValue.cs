﻿namespace GameLib.Events
{
    public class PairOfCharacteristicAndValue
    {
        public Characteristic Characteristic;
        public int Count;
        public override string ToString()
        {
            string stringCharacteristic;
            switch (Characteristic)
            {
                case Characteristic.Mana:
                    stringCharacteristic = "Мана";
                    break;
                case Characteristic.Faith:
                    stringCharacteristic = "Вера";
                    break;
                case Characteristic.Army:
                    stringCharacteristic = "Армия";
                    break;
                default:
                    stringCharacteristic = "Народ";
                    break;
            }
            return $"{stringCharacteristic}: {(Count > 0 ? "+" : "")}{Count}";
        }
    }
}