﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using GameLib;
using GameLib.Solutions;
using GameLib.Events;
using GameLib.ForSaveAndLoad;
using System.IO;

namespace ProjectGame
{
    public partial class MainWindow : Window
    {
        Game game;
        IEvent newEvent;
        bool gridResultEventIsVisible = false; 

        public MainWindow()
        {
            InitializeComponent();
            game = new Game(new GameLoader(@"..\..\Data\Events\Events.txt", @"..\..\Data\Save\User.txt"), new GameSaver(@"..\..\Data\Save\User.txt"));
        }

        private void StartGame()
        {
            newEvent = game.GetEvent();
            ChangeProgressBar(game.GetCharacteristic());
            if (newEvent is EndEvent)
            {
                Grid_resultEvent.Visibility = Visibility.Visible;
                GridGame.IsEnabled = false;
                tb_resultEvent.Text = newEvent.Text;
                button_resultEvent.Content = "Начать заново";
                game.LoadNewGame();
                StartGame();
                ChangeProgressBar(game.GetCharacteristic());
                return;
            }
            if (newEvent is ResultEvent)
            {
                Choice(0);
                Grid_resultEvent.Visibility = Visibility.Visible;
                GridGame.IsEnabled = false;
                tb_resultEvent.Text = newEvent.Text;
                button_resultEvent.Content = "ОК";
                StartGame();
                return;
            }
            else
            {
                MainText2.Text = newEvent.Text;
                BitmapImage NewCard = new BitmapImage(new Uri(newEvent.FileName, UriKind.RelativeOrAbsolute));
                Card.Source = NewCard;
                var solutions = new List<ISolution>(newEvent.GetSolutions());
                TextLeft.Text = solutions[0].Text.ToString();
                popupText.Text = string.Join("\n", solutions[0].GetSolutionChanges());
                TextRight.Text = solutions[1].Text.ToString();
                popupText2.Text = string.Join("\n", solutions[1].GetSolutionChanges());
            }
        }
        private void Choice(int index)
        {
            var sol = new List<ISolution>(newEvent.GetSolutions());
            sol[index].HandleSolution();
            ChangeProgressBar(game.GetCharacteristic());
        }
        private void ChangeProgressBar(IEnumerable<PairOfCharacteristicAndValue> pair)
        {
            foreach (var item in pair)
            {
                switch (item.Characteristic)
                {
                    case Characteristic.Mana:
                        Mana_ProgressBar.Value = item.Count;
                        break;

                    case Characteristic.Faith:
                        Faith_ProgressBar.Value = item.Count;
                        break;

                    case Characteristic.Army:
                        Army_ProgressBar.Value = item.Count;
                        break;

                    case Characteristic.People:
                        People_ProgressBar.Value = item.Count;
                        break;
                }
            }
        }
        private void ChoiceLeft_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Choice(0);
            StartGame();
        }
        private void ChoiceRight_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Choice(1);
            StartGame();
        }
        private void button_resultEvent_Click(object sender, RoutedEventArgs e)
        {
            Grid_resultEvent.Visibility = Visibility.Collapsed;
            GridGame.IsEnabled = true;
            gridResultEventIsVisible = false;
        }

        #region animation
        private void ChoiceLeft_MouseEnter(object sender, MouseEventArgs e)
        {
            DoubleAnimation x = new DoubleAnimation
            {
                From = 0,
                To = -15,
                Duration = TimeSpan.FromSeconds(0.2)
            };
            Card.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, x);
            TextLeft.Visibility = Visibility.Visible;
        }

        private void ChoiceLeft_MouseLeave(object sender, MouseEventArgs e)
        {
            DoubleAnimation x = new DoubleAnimation
            {
                From = -15,
                To = 0,
                Duration = TimeSpan.FromSeconds(0.2)
            };
            Card.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, x);
            TextLeft.Visibility = Visibility.Hidden;
            Tedavi_Popup.IsOpen = false;
        }
        private void ChoiceRight_MouseEnter(object sender, MouseEventArgs e)
        {
            DoubleAnimation x = new DoubleAnimation
            {
                From = 0,
                To = 15,
                Duration = TimeSpan.FromSeconds(0.2)
            };
            Card.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, x);
            TextRight.Visibility = Visibility.Visible;
        }

        private void ChoiceRight_MouseLeave(object sender, MouseEventArgs e)
        {
            DoubleAnimation x = new DoubleAnimation
            {
                From = 15,
                To = 0,
                Duration = TimeSpan.FromSeconds(0.2)
            };
            Card.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, x);
            TextRight.Visibility = Visibility.Hidden;
            Tedavi_Popup2.IsOpen = false;
        }

        private void ChoiceLeft_MouseMove(object sender, MouseEventArgs e)
        {

            if (!Tedavi_Popup.IsOpen)
                Tedavi_Popup.IsOpen = true;

            var mousePosition = e.GetPosition(ChoiceLeft);
            Tedavi_Popup.HorizontalOffset = mousePosition.X;
            Tedavi_Popup.VerticalOffset = mousePosition.Y;
        }
        private void ChoiceRight_MouseMove(object sender, MouseEventArgs e)
        {
            if (!Tedavi_Popup2.IsOpen)
                Tedavi_Popup2.IsOpen = true;

            var mousePosition = e.GetPosition(ChoiceRight);
            Tedavi_Popup2.HorizontalOffset = mousePosition.X;
            Tedavi_Popup2.VerticalOffset = mousePosition.Y;
        }


        #endregion

        private void buttonNewGame_Click(object sender, RoutedEventArgs e)
        {
            Menu.Visibility = Visibility.Collapsed;
            GridGame.Visibility = Visibility.Visible;
            game.LoadNewGame();
            StartGame();
        }

        private void buttonResume_Click(object sender, RoutedEventArgs e)
        {
            Menu.Visibility = Visibility.Collapsed;
            GridGame.Visibility = Visibility.Visible;
            if (gridResultEventIsVisible)
            {
                Grid_resultEvent.Visibility = Visibility.Visible;
            }
            game.LoadSavedGame();
            StartGame();
        }

        private void buttonHelp_Click(object sender, RoutedEventArgs e)
        {
            buttonNewGame.Visibility = Visibility.Collapsed;
            buttonResume.Visibility = Visibility.Collapsed;
            buttonHelp.Visibility = Visibility.Collapsed;
            buttonExit.Visibility = Visibility.Collapsed;
            scroll.Visibility = Visibility.Visible;
            tbRules.Text = File.ReadAllText(@"..\..\Data\Rules\Rules.txt");
        }

        private void btRules_Click(object sender, RoutedEventArgs e)
        {
            buttonNewGame.Visibility = Visibility.Visible;
            buttonResume.Visibility = Visibility.Visible;
            buttonHelp.Visibility = Visibility.Visible;
            buttonExit.Visibility = Visibility.Visible;
            scroll.Visibility = Visibility.Collapsed;
        }

        private void GridGame_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Menu.Visibility = Visibility.Visible;
                GridGame.Visibility = Visibility.Hidden;
                if (Grid_resultEvent.Visibility == Visibility.Visible)
                {
                    Grid_resultEvent.Visibility = Visibility.Collapsed;
                    gridResultEventIsVisible = true;
                }
            }
        }

        private void buttonExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
