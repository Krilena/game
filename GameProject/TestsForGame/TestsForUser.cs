﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using GameLib;
using GameLib.Events;
using Newtonsoft.Json;

namespace TestsForGame
{
    [TestClass]
    public class TestsForUser
    {
        [TestMethod]
        public void JsonSave()
        {
            var characteristics = new Dictionary<Characteristic, int>
            {
                {Characteristic.Mana, 12},
                {Characteristic.People, 75},
                {Characteristic.Faith, 90},
                {Characteristic.Army, 5}
            };

            var character = new List<PairOfCharacteristicAndValue>();
            foreach (var item in characteristics)
                character.Add(new PairOfCharacteristicAndValue { Characteristic = item.Key, Count = item.Value });

            var user = new User(character);
            var json = JsonConvert.SerializeObject(user);
            user = JsonConvert.DeserializeObject<User>(json);
            var list = new List<PairOfCharacteristicAndValue>(user.GetCharacteristic());

            foreach (var pairOfCharacteristicAndValue in list)
            {
                Assert.AreEqual(characteristics[pairOfCharacteristicAndValue.Characteristic],
                    pairOfCharacteristicAndValue.Count);
            }
        }

        [TestMethod]
        public void CorrectnessOfCreationByDefault()
        {
            var characteristics = new Dictionary<Characteristic, int>
            {
                {Characteristic.Mana, 50},
                {Characteristic.People, 50},
                {Characteristic.Faith, 50},
                {Characteristic.Army, 50}
            };

            var user = new User();
            var userCharacteristics = new List<PairOfCharacteristicAndValue>(user.GetCharacteristic());
            Assert.AreEqual(4, userCharacteristics.Count);

            foreach (var item in userCharacteristics)
            {
                Assert.AreEqual(characteristics[item.Characteristic], item.Count);
            }
        }

        [TestMethod]
        public void CorrectnessOfCreationWithChangedCharacteristics()
        {
            var characteristics = new Dictionary<Characteristic, int>
            {
                {Characteristic.Mana, 12},
                {Characteristic.People, 75},
                {Characteristic.Faith, 90},
                {Characteristic.Army, 5}
            };

            var character = new List<PairOfCharacteristicAndValue>();
            foreach (var item in characteristics)
                character.Add(new PairOfCharacteristicAndValue { Characteristic = item.Key, Count = item.Value });

            var user = new User(character);
            var userCharacteristics = new List<PairOfCharacteristicAndValue>(user.GetCharacteristic());
            Assert.AreEqual(4, userCharacteristics.Count);

            foreach (var item in userCharacteristics)
            {
                Assert.AreEqual(characteristics[item.Characteristic], item.Count);
            }
        }

        [TestMethod]
        public void CorrectnessOfChangeInCharacteristics()
        {
            var characteristics = new Dictionary<Characteristic, int>
            {
                {Characteristic.Mana, 38},
                {Characteristic.People, 55},
                {Characteristic.Faith, 10},
                {Characteristic.Army, 50}
            };

            var changes = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue{Characteristic = Characteristic.Mana,Count = -12},
                new PairOfCharacteristicAndValue{Characteristic = Characteristic.People,Count = 5},
                new PairOfCharacteristicAndValue{Characteristic = Characteristic.Faith,Count = -40}
            };

            var user = new User();
            user.ChangeСharacteristics(changes);
            var userCharacteristics = user.GetCharacteristic();

            foreach (var item in userCharacteristics)
            {
                Assert.AreEqual(characteristics[item.Characteristic], item.Count);
            }
        }
    }
}
