﻿using System.Collections.Generic;
using GameLib.Events;
using GameLib.Solutions;

namespace TestsForGame
{
    public class TestEvent : IEvent
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public string FileName { get; set; }

        public int TimeToRollback { get; set; }
        public List<ISolution> Solutions { get; set; }
        public IEnumerable<ISolution> GetSolutions()
        {
            return Solutions;
        }
    }
}