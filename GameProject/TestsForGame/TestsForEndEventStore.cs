﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameLib.Events;
using GameLib;
using GameLib.EventsStore;

namespace TestsForGame
{
    [TestClass]
    public class TestsForEndEventStore
    {
        private Func<long, IEvent> _getEventById;

        private Dictionary<long,IEvent> _endEvents = new Dictionary<long, IEvent>
            {
            {1, new EndEvent
                {Id = 1,Text = "Mana",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100}}}
                } },
            {2, new EndEvent
                 {Id = 2,Text = "Faith",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0}}}
                } },
            {3, new EndEvent
                {Id = 3,Text = "People",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100}}}
                } },
            {4,   new EndEvent
                {Id = 4,Text = "Army",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 0}}}
                } }
            };

        [TestMethod]
        public void CorrectnessOfDefinitionThatNotTimeForEvent()
        {
            _getEventById += delegate (long id) { return _endEvents[id]; };

            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 99},
            };

            var endEventStore = new EndEventsStore(new List<long> { 1,2,3,4},_getEventById);
            Assert.IsFalse(endEventStore.IsTimeForEndEvent(characteristic));
        }

        [TestMethod]
        public void CorrectnessOfDefinitionThatTimeForEvent()
        {
            _getEventById += delegate (long id) { return _endEvents[id]; };
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
            };

            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3, 4 }, _getEventById);
            Assert.IsTrue(endEventStore.IsTimeForEndEvent(characteristic));
        }

        [TestMethod]
        public void СorrectnessOfGetingWhenMana()
        {
            _getEventById += delegate (long id) { return _endEvents[id]; };
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
            };

            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3, 4 }, _getEventById);

            Assert.AreEqual("Mana", endEventStore.GetEvent(characteristic).Text);
        }

        [TestMethod]
        public void СorrectnessOfGetingWhenFaith()
        {
            _getEventById += delegate (long id) { return _endEvents[id]; };

            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 50},
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3, 4 }, _getEventById);

            Assert.AreEqual("Faith", endEventStore.GetEvent(characteristic).Text);
        }

        [TestMethod]
        public void СorrectnessOfGetingWhenArmy()
        {
            _getEventById += delegate (long id) { return _endEvents[id]; };

            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 0},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 50},
            };

            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3, 4 }, _getEventById);

            Assert.AreEqual("Army", endEventStore.GetEvent(characteristic).Text);
        }

        [TestMethod]
        public void СorrectnessOfGetingWhenPeople()
        {
            _getEventById += delegate (long id) { return _endEvents[id]; };

            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 50},
            };

            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3, 4 }, _getEventById);

            Assert.AreEqual("People", endEventStore.GetEvent(characteristic).Text);
        }
    }
}