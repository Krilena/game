﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLib.Events;
using GameLib.EventsStore;
using GameLib.Solutions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameLib.ForSaveAndLoad;

namespace TestsForGame
{
    [TestClass]
    public class TestsForDeferredEventsStore
    {
        private Func<long, IEvent> _getEventById;

        [TestMethod]
        public void CorrectnessGetEvent()
        {
            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new DeferredEvent{Id = 1} },
                     { 2,new DeferredEvent{Id = 2} },
                 };
                return events[id];
            };

            var store = new DeferredEventsStore(new List<long> { 2 }, _getEventById);
            var @event = store.GetEvent(1) as DeferredEvent;

            Assert.AreEqual(2, @event.Id);
        }

        [TestMethod]
        public void CorrectnessOfSendingEventToBasket()
        {
            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new DeferredEvent{Id = 1, TimeToRollback = 5} },
                     { 2,new DeferredEvent{Id = 2, TimeToRollback = 5} },
                     { 3,new DeferredEvent{Id = 3, TimeToRollback = 5} },
                 };
                return events[id];
            };

            var eventsList = new List<long> { 1, 2, 3 };

            var store = new DeferredEventsStore(eventsList, _getEventById);

            store.GetEvent(1);
            store.GetEvent(2);
            store.GetEvent(3);

            Assert.AreEqual(0, eventsList.Count);
        }

        [TestMethod]
        public void CorrectnessOfReturnOfEventTypes()
        {
            var eventsList = new List<long> { 1, 2, 3 };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new DeferredEvent{Id = 1, Solutions = new List<SolutionWithSingleEvent>{new SolutionWithSingleEvent{ResultEvent = new StorageOfCleanedEvent{Id = 4}} } } },
                     { 2,new DeferredEvent{Id = 2, Solutions = new List<SolutionWithSingleEvent>{new SolutionWithSingleEvent{ResultEvent = new StorageOfCleanedEvent { Id = 5 } } }} },
                     { 3,new DeferredEvent{Id = 3, Solutions = new List<SolutionWithSingleEvent>{new SolutionWithSingleEvent{ResultEvent = new StorageOfCleanedEvent { Id = 6 } } }} },
                    {4,new ResultEvent{Id =4} },
                    {5,new ResultEvent{Id =5} },
                    {6,new ResultEvent{Id =6} }
                 };
                return events[id];
            };

            var store = new DeferredEventsStore(eventsList, _getEventById);

            var @event = store.GetEvent(1);
            Assert.IsInstanceOfType(@event, typeof(DeferredEvent));
            var result = (@event.GetSolutions().ToArray()[0] as SolutionWithSingleEvent).ResultEvent;
            store.AddResultToExpect(result.Id,result.WaitingTime);
            Assert.IsInstanceOfType(store.GetEvent(2), typeof(ResultEvent));

            @event = store.GetEvent(3);
            Assert.IsInstanceOfType(@event, typeof(DeferredEvent));
            result = (@event.GetSolutions().ToArray()[0] as SolutionWithSingleEvent).ResultEvent;
            store.AddResultToExpect(result.Id, result.WaitingTime);
            Assert.IsInstanceOfType(store.GetEvent(4), typeof(ResultEvent));

            @event = store.GetEvent(5);
            Assert.IsInstanceOfType(@event, typeof(DeferredEvent));
            result = (@event.GetSolutions().ToArray()[0] as SolutionWithSingleEvent).ResultEvent;
            store.AddResultToExpect(result.Id, result.WaitingTime);
            Assert.IsInstanceOfType(store.GetEvent(6), typeof(ResultEvent));
        }

        [TestMethod]
        public void EventsAreReturnedFromBasketAfterCertainTime()
        {
            var eventsList = new List<long> { 1, 2, 3 };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new DeferredEvent{Id = 1, TimeToRollback = 3} },
                     { 2,new DeferredEvent{Id = 2,TimeToRollback = 3 } },
                     { 3,new DeferredEvent{Id = 3,TimeToRollback = 3}},
                 };
                return events[id];
            };

            var store = new DeferredEventsStore(eventsList, _getEventById);

            var selectEvent = new List<DeferredEvent>
            {
                store.GetEvent(1) as DeferredEvent,
                store.GetEvent(2) as DeferredEvent,
                store.GetEvent(3) as DeferredEvent
            };

            var newEvent = store.GetEvent(4) as DeferredEvent;
            Assert.AreEqual(selectEvent[0].Id, newEvent.Id);
            newEvent = store.GetEvent(5) as DeferredEvent;
            Assert.AreEqual(selectEvent[1].Id, newEvent.Id);
            newEvent = store.GetEvent(6) as DeferredEvent;
            Assert.AreEqual(selectEvent[2].Id, newEvent.Id);
        }

        [TestMethod]
        public void CorrectnessWorkIfListWithInstantEventIsEmpty()
        {
            var eventsList = new List<long> { 1};

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new DeferredEvent{Id = 1, TimeToRollback = 5} }
                 };
                return events[id];
            };

            var store = new DeferredEventsStore(eventsList, _getEventById);

            store.GetEvent(1);

            Assert.IsNotNull(store.GetEvent(2));
        }

        [TestMethod]
        public void FalseIfTimeForEventNotCome()
        {
            var store = new DeferredEventsStore(new List<long> { 1 }, _getEventById);
            Assert.IsFalse(store.IsTimeForDeferredEvent(2));
        }

        [TestMethod]
        public void TrueIfTimeForEventCome()
        {
            var store = new DeferredEventsStore(new List<long> { 1 }, _getEventById);
            Assert.IsTrue(store.IsTimeForDeferredEvent(14));
        }

        [TestMethod]
        public void FalseIfTimeForResultEventNotCome()
        {
            var memento = new DeferredEventsMemento(new List<long> { 1 }, null, new StorageOfCleanedEvent { WaitingTime = 2 }, 1, 10);
            var store = new DeferredEventsStore(memento, _getEventById);
            Assert.IsFalse(store.IsTimeForDeferredEvent(2));
        }

        [TestMethod]
        public void TrueIfTimeForResultEventCome()
        {
            var memento = new DeferredEventsMemento(new List<long> { 1 }, null, new StorageOfCleanedEvent { WaitingTime = 2 }, 1, 10);
            var store = new DeferredEventsStore(memento, _getEventById);
            store.IsTimeForDeferredEvent(2);
            store.IsTimeForDeferredEvent(3);
            Assert.IsTrue(store.IsTimeForDeferredEvent(4));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenListIsEmpty()
        {
            var store = new DeferredEventsStore(new List<long>(), _getEventById);
        }
    }
}