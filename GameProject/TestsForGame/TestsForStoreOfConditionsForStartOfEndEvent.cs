﻿using System.Collections.Generic;
using System;
using GameLib;
using GameLib.Events;
using GameLib.EventsStore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsForGame
{
    [TestClass]
    public class TestsForStoreOfConditionsForStartOfEndEvent
    {
        private Func<long, IEvent> _getEventById;

        [TestMethod]
        public void CorrectnessOfWorkWith_FullMana()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
            };

            _getEventById += delegate (long id)
            {

                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 100}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1,2,3},_getEventById);

            Assert.AreEqual(1, (endEventStore.GetEvent(characteristic)as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_EmptyMana()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 0},
            };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 0}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 0}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3 }, _getEventById);

            Assert.AreEqual(1, (endEventStore.GetEvent(characteristic) as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_FullFaith()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 100},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 50},
            }; 

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 100}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3 }, _getEventById);

            Assert.AreEqual(2, (endEventStore.GetEvent(characteristic) as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_EmptyFaith()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
            };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 0}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 0}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3 }, _getEventById);

            Assert.AreEqual(2, (endEventStore.GetEvent(characteristic) as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_FullArmy()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 100},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
            };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 100}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 100}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3 }, _getEventById);

            Assert.AreEqual(1, (endEventStore.GetEvent(characteristic) as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_EmptyArmy()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 0},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
            };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 0}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 0}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3 }, _getEventById);

            Assert.AreEqual(1, (endEventStore.GetEvent(characteristic) as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_FullPeople()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 50},
            };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 100}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3 }, _getEventById);

            Assert.AreEqual(3, (endEventStore.GetEvent(characteristic) as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_EmptyPeople()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 50},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 0},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
            };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 0}}}
                }},
                    {2, new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 100}}}
                } },
                    {3, new EndEvent
                {Id = 3,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 0}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3 }, _getEventById);

            Assert.AreEqual(3, (endEventStore.GetEvent(characteristic) as EndEvent).Id);
        }

        [TestMethod]
        public void CorrectnessOfWorkWith_SeveralEvent()
        {
            var characteristic = new List<PairOfCharacteristicAndValue>
            {
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 101},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 101},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = -1},
                new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = -1},
            };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
            {
                    {1, new EndEvent
                {Id = 1,
                    Text = "True",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 100},
                            new PairOfCharacteristicAndValue{ Characteristic = Characteristic.Faith, Count = 0} }}
                } },
                    {2,   new EndEvent
                {Id = 2,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0}}}
                } },
                    {3, new EndEvent
                {Id =3,
                    Text = "True",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0},
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 0}}}
                } },
                    {4, new EndEvent
                {Id = 4,
                    Text = "True",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.People, Count = 100},
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Army, Count = 100}}}
                } },
                    {5, new EndEvent
                {Id = 5,
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 0}}}
                } },
                    {6,  new EndEvent
                {Id = 6,
                    Text = "True",
                    NecessaryConditions = new ConditionsForStartOfEndEvent {
                        Conditions = new List<PairOfCharacteristicAndValue> {
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Faith, Count = 100},
                            new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 0}}}
                } }
            };
                return events[id];
            };
            var endEventStore = new EndEventsStore(new List<long> { 1, 2, 3,4,5,6 }, _getEventById);

            for (var i = 0; i < 10; i++)
                Assert.AreEqual("True", endEventStore.GetEvent(characteristic).Text);
        }
    }
}