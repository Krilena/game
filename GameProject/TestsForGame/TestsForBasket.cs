﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using GameLib;
using GameLib.Events;
using GameLib.EventsStore;
using GameLib.Solutions;
using Newtonsoft.Json;

namespace TestsForGame
{
    [TestClass]
    public class TestsForBasket
    {
        [TestMethod]
        public void CorrectnessAddToBasket()
        {
            var events = new List<TestEvent>
            {
                new TestEvent {Id =1, TimeToRollback = 1},
                new TestEvent {Id =2, TimeToRollback = 1},
                new TestEvent {Id =3, TimeToRollback = 1}
            };

            var basket = new Basket();

            basket.AddEvent(events[0].Id, events[0].TimeToRollback);
            basket.AddEvent(events[1].Id, events[1].TimeToRollback);
            basket.AddEvent(events[2].Id, events[2].TimeToRollback);

            var returnEvents = basket.ReturnEventsFromBasket();

            for (var i = 0; i < events.Count; i++)
                Assert.AreEqual(events[i].Id, returnEvents[i]);

            returnEvents = basket.ReturnEventsFromBasket();
            Assert.AreEqual(0, returnEvents.Count);
        }

        [TestMethod]
        public void CorrectnessWorkOfBasket()
        {
            var events = new List<TestEvent>
            {
                new TestEvent {Id =1, TimeToRollback = 1},
                new TestEvent {Id =2, TimeToRollback = 1},
                new TestEvent {Id =3, TimeToRollback = 1},
                new TestEvent {Id =4, TimeToRollback = 2}
            };

            var storage = new List<StorageOfCleanedEvent>
            {
                new StorageOfCleanedEvent {Id =1 , WaitingTime = 1},
                new StorageOfCleanedEvent {Id =2, WaitingTime = 2},
                new StorageOfCleanedEvent {Id =3, WaitingTime = 2},
                new StorageOfCleanedEvent {Id =4, WaitingTime = 3},
            };

            var basket = new Basket(storage);

            var returnEvents = basket.ReturnEventsFromBasket();
            Assert.AreEqual(1, returnEvents.Count);
            Assert.AreEqual(events[0].Id, returnEvents[0]);

            returnEvents = basket.ReturnEventsFromBasket();
            Assert.AreEqual(2, returnEvents.Count);
            Assert.AreEqual(events[1].Id, returnEvents[0]);
            Assert.AreEqual(events[2].Id, returnEvents[1]);

            returnEvents = basket.ReturnEventsFromBasket();
            Assert.AreEqual(1, returnEvents.Count);
            Assert.AreEqual(events[3].Id, returnEvents[0]);

            returnEvents = basket.ReturnEventsFromBasket();
            Assert.AreEqual(0, returnEvents.Count);
        }
    }
}