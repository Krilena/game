﻿using GameLib;
using GameLib.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsForGame
{
    [TestClass]
    public class TestsForPairOfCharacteristicAndValue
    {
        [TestMethod]
        public void FormatCharacteristic_PositiveMana()
        {
            var characteristic = new PairOfCharacteristicAndValue {Characteristic = Characteristic.Mana, Count = 10};
            Assert.AreEqual("Мана: +10",characteristic.ToString());
        }

        [TestMethod]
        public void FormatCharacteristic_NegativeMana()
        {
            var characteristic = new PairOfCharacteristicAndValue { Characteristic = Characteristic.Mana, Count = -10 };
            Assert.AreEqual("Мана: -10", characteristic.ToString());
        }

        [TestMethod]
        public void FormatCharacteristic_PositiveFaith()
        {
            var characteristic = new PairOfCharacteristicAndValue { Characteristic = Characteristic.Faith, Count = 10 };
            Assert.AreEqual("Вера: +10", characteristic.ToString());
        }

        [TestMethod]
        public void FormatCharacteristic_NegativeFaith()
        {
            var characteristic = new PairOfCharacteristicAndValue { Characteristic = Characteristic.Faith, Count = -10 };
            Assert.AreEqual("Вера: -10", characteristic.ToString());
        }

        [TestMethod]
        public void FormatCharacteristic_PositiveArmy()
        {
            var characteristic = new PairOfCharacteristicAndValue { Characteristic = Characteristic.Army, Count = 10 };
            Assert.AreEqual("Армия: +10", characteristic.ToString());
        }

        [TestMethod]
        public void FormatCharacteristic_NegativeArmy()
        {
            var characteristic = new PairOfCharacteristicAndValue { Characteristic = Characteristic.Army, Count = -10 };
            Assert.AreEqual("Армия: -10", characteristic.ToString());
        }

        [TestMethod]
        public void FormatCharacteristic_PositivePeople()
        {
            var characteristic = new PairOfCharacteristicAndValue { Characteristic = Characteristic.People, Count = 10 };
            Assert.AreEqual("Народ: +10", characteristic.ToString());
        }

        [TestMethod]
        public void FormatCharacteristic_NegativePeople()
        {
            var characteristic = new PairOfCharacteristicAndValue { Characteristic = Characteristic.People, Count = -10 };
            Assert.AreEqual("Народ: -10", characteristic.ToString());
        }
    }
}