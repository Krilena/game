﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using GameLib;
using GameLib.Events;
using GameLib.EventsStore;
using Newtonsoft.Json;

namespace TestsForGame
{
    [TestClass]
    public class TestsForInstantEventsStore
    {
        private Func<long, IEvent> _getEventById;

        [TestMethod]
        public void CorrectnessOfSendingEventToBasket()
        {
            var eventsList = new List<long> { 1, 2, 3 };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new InstantEvent{Id = 1,TimeToRollback = 3} },
                     { 2,new InstantEvent{Id = 2,TimeToRollback = 3} },
                     { 3,new InstantEvent{Id = 3,TimeToRollback = 3} }
                 };
                return events[id];
            };

            var store = new InstantEventsStore(eventsList, _getEventById);
            store.GetEvent();
            store.GetEvent();
            store.GetEvent();

            Assert.AreEqual(0, eventsList.Count);
        }

        [TestMethod]
        public void EventsAreReturnedFromBasketAfterCertainTime()
        {
            var eventsList = new List<long> { 1, 2, 3 };

            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new InstantEvent{Id = 1,TimeToRollback = 3} },
                     { 2,new InstantEvent{Id = 2,TimeToRollback = 3} },
                     { 3,new InstantEvent{Id = 3,TimeToRollback = 3} }
                 };
                return events[id];
            };
            var store = new InstantEventsStore(eventsList, _getEventById);

            var selectEvent = new List<InstantEvent>
            {
                store.GetEvent() as InstantEvent, store.GetEvent() as InstantEvent, store.GetEvent() as InstantEvent
            };

            var newEvent = store.GetEvent() as InstantEvent;
            Assert.AreEqual(selectEvent[0].Id, newEvent.Id);
            newEvent = store.GetEvent() as InstantEvent;
            Assert.AreEqual(selectEvent[1].Id, newEvent.Id);
            newEvent = store.GetEvent() as InstantEvent;
            Assert.AreEqual(selectEvent[2].Id, newEvent.Id);
        }

        [TestMethod]
        public void CorrectnessWorkIfListWithInstantEventIsEmpty()
        {
            _getEventById += delegate (long id)
            {
                var events = new Dictionary<long, IEvent>
                 {
                     { 1,new InstantEvent{Id = 1,TimeToRollback = 5} },
                 };
                return events[id];
            };
            var store = new InstantEventsStore(new List<long> { 1 }, _getEventById) ;

            store.GetEvent();

            Assert.IsNotNull(store.GetEvent());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenListIsEmpty()
        {
            var store = new InstantEventsStore(new List<long>(), _getEventById);
        }
    }
}