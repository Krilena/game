﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GameLib;
using GameLib.Events;
using GameLib.Solutions;

namespace EventCreator
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class InstantSolutionCreator : Window
    {
        private SolutionWithInstantChanges _solution;
        public InstantSolutionCreator(SolutionWithInstantChanges solution, string message)
        {
            InitializeComponent();
            _solution = solution;
            SolutionTextBlock.Text = message;
            createButton.IsEnabled = false;
            foreach (var item in grid.Children.OfType<TextBox>())
            {
                item.TextChanged += TextBox_TextChanged;
            }
        }

        private void EnableButton()
        {
            bool t = InstantEventTextTextBox.Text == string.Empty;
            bool tt = (InstantEventArmyValue.Text + InstantEventManaValue.Text + InstantEventFaithValue.Text + InstantEventPeopleValue.Text) == string.Empty;
            if (!t && !tt) createButton.IsEnabled = true;
            else createButton.IsEnabled = false;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableButton();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _solution.Text = InstantEventTextTextBox.Text;
            var changes = new List<PairOfCharacteristicAndValue>();
            if (InstantEventManaValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Mana, Count = int.Parse(InstantEventManaValue.Text)});
            }
            if (InstantEventArmyValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Army, Count = int.Parse(InstantEventArmyValue.Text) });
            }
            if (InstantEventPeopleValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.People, Count = int.Parse(InstantEventPeopleValue.Text) });
            }
            if (InstantEventFaithValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Faith, Count = int.Parse(InstantEventFaithValue.Text) });
            }
            _solution.Changes = changes;                 
            MessageBox.Show("Выбор успешно создан");            
            Close();
        }
    }
}
