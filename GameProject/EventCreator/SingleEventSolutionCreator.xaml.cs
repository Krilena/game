﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GameLib;
using GameLib.Events;
using GameLib.EventsStore;
using GameLib.Solutions;

namespace EventCreator
{
    /// <summary>
    /// Логика взаимодействия для SingleEventSolutionCreator.xaml
    /// </summary>
    public partial class SingleEventSolutionCreator : Window
    {
        private SolutionWithSingleEvent _solution;
        private Dictionary<long, IEvent> _events;
        public SingleEventSolutionCreator(SolutionWithSingleEvent solutionWithSingleEvent, string message,   Dictionary<long, IEvent> events)
        {
            InitializeComponent();
            _solution = solutionWithSingleEvent;
            _events = events;
            createButton.IsEnabled = false;
            foreach (var item in grid.Children.OfType<TextBox>())
            {
                item.TextChanged += TextBox_TextChanged;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool setext = SingleEventTextTextBox.Text == string.Empty;
            bool t = (SingleEventManaValue.Text + SingleEventArmyValue.Text + SingleEventFaithValue.Text + SingleEventPeopleValue.Text) == string.Empty;
            bool retext = ResultEventTextBox.Text == string.Empty;
            bool rewt = ResultEventWaitingTime.Text == string.Empty;
            bool refn = ResultEventFileName.Text == string.Empty;
            if (!setext && !t && !retext && !rewt && !refn) createButton.IsEnabled = true;
            else createButton.IsEnabled = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _solution.Text = SingleEventTextTextBox.Text;            
            var firstInstantEvent = new SolutionWithInstantChanges();          
            var changes = new List<PairOfCharacteristicAndValue>();
            var resultEvent = new ResultEvent();           
            if (SingleEventManaValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Mana, Count = int.Parse(SingleEventManaValue.Text) });
            }
            if (SingleEventArmyValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Army, Count = int.Parse(SingleEventArmyValue.Text) });
            }
            if (SingleEventPeopleValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.People, Count = int.Parse(SingleEventPeopleValue.Text) });
            }
            if (SingleEventFaithValue.Text != string.Empty)
            {
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Faith, Count = int.Parse(SingleEventFaithValue.Text) });
            }
            _solution.Changes = changes;
            resultEvent.Text = ResultEventTextBox.Text;
            resultEvent.WaitingTime = int.Parse(ResultEventWaitingTime.Text);
            var solutionWithInstantChanges = new SolutionWithInstantChanges();
            InstantSolutionCreator instantSolutionCreator = new InstantSolutionCreator(solutionWithInstantChanges, "Создайте событие для ResultEvent");
            Hide();
            instantSolutionCreator.ShowDialog();
            Show();
            resultEvent.Solution = solutionWithInstantChanges;
            resultEvent.FileName = ResultEventFileName.Text;

            resultEvent.Id = GetNewId();
            _events.Add(resultEvent.Id,resultEvent);

            _solution.ResultEvent = new StorageOfCleanedEvent{ Id = resultEvent.Id,WaitingTime = resultEvent.WaitingTime};
            MessageBox.Show("Выбор успешно создан");
            Close();
        }
        private long GetNewId()
        {
            long id;
            do
            {
                id = new Random(DateTime.Now.Millisecond).Next();
            } while (_events.ContainsKey(id));

            return id;
        }
    }
}
