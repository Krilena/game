﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GameLib;
using GameLib.Events;
using GameLib.Solutions;
using Newtonsoft.Json;

namespace EventCreator
{
    /// <summary>
    /// Логика взаимодействия для EndEventCreator.xaml
    /// </summary>
    public partial class EndEventCreator : Window
    {
        private Dictionary<long, IEvent> _events;

        public EndEventCreator(Dictionary<long, IEvent> events)
        {
            InitializeComponent();
            _events = events;
            foreach (var item in grid.Children.OfType<TextBox>())
            {
                item.TextChanged += ButtonEnable;
            }
            foreach (var item in grid.Children.OfType<ComboBox>())
            {
                item.SelectionChanged += ButtonE1nable;
            }
        }

        private void ButtonE1nable(object sender, SelectionChangedEventArgs e)
        {
            bool t = !grid.Children.OfType<TextBox>().Any(x => string.IsNullOrEmpty(x.Text));
            bool tm = EndEventConditionsManaValue.SelectedIndex > -1;
            bool ta = EndEventConditionsArmyValue.SelectedIndex > -1;
            bool tf = EndEventConditionsFaithValue.SelectedIndex > -1;
            bool tp = EndEventConditionsPeopleValue.SelectedIndex > -1;
            if (t && (!tm || !ta || tf || tp)) createButton.IsEnabled = true;
            else createButton.IsEnabled = false;
        }

        private void ButtonEnable(object sender, TextChangedEventArgs e)
        {
            bool t = !grid.Children.OfType<TextBox>().Any(x => string.IsNullOrEmpty(x.Text));
            bool tm = EndEventConditionsManaValue.SelectedIndex > -1;
            bool ta = EndEventConditionsArmyValue.SelectedIndex > -1;
            bool tf = EndEventConditionsFaithValue.SelectedIndex > -1;
            bool tp = EndEventConditionsPeopleValue.SelectedIndex > -1;
            if (t && (!tm || !ta || tf || tp)) createButton.IsEnabled = true;
            else createButton.IsEnabled = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            EndEvent endEvent = new EndEvent();
            var conditions = new ConditionsForStartOfEndEvent();
            conditions.Conditions = GetConditions();
            endEvent.NecessaryConditions = conditions;
            endEvent.FileName = EndEventFileName.Text;
            endEvent.Text = EndEventTextTextBox.Text;

            endEvent.Id = GetNewId();
            _events.Add(endEvent.Id,endEvent);

            MessageBox.Show("Событие успешно создано");
            Clear();
        }
        
        private long GetNewId()
        {
            long id;
            do
            {
                id = new Random(DateTime.Now.Millisecond).Next();
            } while (_events.ContainsKey(id));

            return id;
        }

        private void Clear()
        {
            EndEventFileName.Text = "";
            EndEventTextTextBox.Text = "";
            EndEventConditionsManaValue.SelectedIndex = -1;
            EndEventConditionsFaithValue.SelectedIndex = -1;
            EndEventConditionsArmyValue.SelectedIndex = -1;
            EndEventConditionsPeopleValue.SelectedIndex = -1;
        }

        private List<PairOfCharacteristicAndValue> GetConditions()
        {
            var conditionsList = new List<PairOfCharacteristicAndValue>();

            foreach (var condition in ConvertComboBox(EndEventConditionsManaValue, Characteristic.Mana))
                conditionsList.Add(condition);
            foreach (var condition in ConvertComboBox(EndEventConditionsArmyValue, Characteristic.Army))
                conditionsList.Add(condition);
            foreach (var condition in ConvertComboBox(EndEventConditionsFaithValue, Characteristic.Faith))
                conditionsList.Add(condition);
            foreach (var condition in ConvertComboBox(EndEventConditionsPeopleValue, Characteristic.People))
                conditionsList.Add(condition);

            return conditionsList;
        }

        private List<PairOfCharacteristicAndValue> ConvertComboBox(ComboBox comboBox, Characteristic characteristic)
        {
            var conditionsList = new List<PairOfCharacteristicAndValue>();
            switch (comboBox.SelectedItem as string)
            {
                case "0":
                    conditionsList.Add(new PairOfCharacteristicAndValue { Characteristic = characteristic, Count = 0 });
                    break;
                case "100":
                    conditionsList.Add(new PairOfCharacteristicAndValue { Characteristic = characteristic, Count = 100 });
                    break;
                case "0/100":
                    conditionsList.Add(new PairOfCharacteristicAndValue { Characteristic = characteristic, Count = 0 });
                    conditionsList.Add(new PairOfCharacteristicAndValue { Characteristic = characteristic, Count = 100 });
                    break;
            }

            return conditionsList;
        }
    }
}
