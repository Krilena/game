﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;
using GameLib;
using GameLib.Events;
using GameLib.Solutions;

namespace EventCreator
{
    public partial class EventCreatorWindow : Window
    {
        public EventCreatorWindow()
        {
            InitializeComponent();
            EventType.Items.Add("DeferredEvent");
            EventType.Items.Add("InstantEvent");
        }

        private void IsCharacteristicSwitched(object sender, RoutedEventArgs e)
        {
            var checkBox = sender as CheckBox;
            if (checkBox == ArmyCheckBox)
            {
                ChangeCharacteristicTextBox(e.RoutedEvent == ToggleButton.CheckedEvent, ArmyTextBox);
            }
            if (checkBox == PeopleCheckBox)
            {
                ChangeCharacteristicTextBox(e.RoutedEvent == ToggleButton.CheckedEvent, PeopleTextBox);
            }
            if (checkBox == ChurchCheckBox)
            {
                ChangeCharacteristicTextBox(e.RoutedEvent == ToggleButton.CheckedEvent, ChurchTextBox);
            }
            if (checkBox == TreasuryCheckBox)
            {
                ChangeCharacteristicTextBox(e.RoutedEvent == ToggleButton.CheckedEvent, TreasuryTextBox);
            }
        }

        private void ChangeCharacteristicTextBox(bool isEnabled, TextBox textBox)
        {
            textBox.Text = String.Empty;
            textBox.IsEnabled = isEnabled;
        }

        private void CharacteristicTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CreateEventButton_Click(object sender, RoutedEventArgs e)
        {
            var eventType = EventType.SelectedItem.ToString();
            var eventText = EventText.Text;

            if (eventType == "DeferredEvent")
            {
                var Event = new DeferredEvent();
                Event.Text = eventText;
                var solution = new SolutionWithSingleEvent();
                solution.Text = "Что сделать";
                //solution.ResultEvent = new ResultEvent();
                //solution.ResultEvent.Text = "Что произошло";
                //solution.ResultEvent.Solution.Changes = GetChanges();
                Event.Solutions = new List<SolutionWithSingleEvent> { solution };

                XmlSerializer formatter = new XmlSerializer(typeof(List<DeferredEvent>));
                using (var fs = new FileStream("deferredEventList.xml", FileMode.OpenOrCreate))
                {
                    List<DeferredEvent> deferredEventList = (List<DeferredEvent>)formatter.Deserialize(fs);

                    deferredEventList.Add(Event);
                    formatter.Serialize(fs, deferredEventList);
                }
            }
            else if (eventType == "InstantEvent")
            {
                var Event = new InstantEvent();
                Event.Text = eventText;
                var solution = new SolutionWithInstantChanges();
                solution.Text = "Что сделать";
                solution.Text = "Что произошло";
                solution.Changes = GetChanges();
                Event.Solutions = new List<SolutionWithInstantChanges> { solution };

                XmlSerializer formatter = new XmlSerializer(typeof(List<InstantEvent>));
                using (var fs = new FileStream("instantEventList.xml", FileMode.OpenOrCreate))
                {
                    //List<InstantEvent> instantEventList = (List<InstantEvent>)formatter.Deserialize(fs);
                    var instantEventList = new List<InstantEvent>();
                    instantEventList.Add(Event);
                    formatter.Serialize(fs, instantEventList);
                }
            }
        }

        private List<PairOfCharacteristicAndValue> GetChanges()
        {
            var changes = new List<PairOfCharacteristicAndValue>();
            if (TreasuryCheckBox.IsChecked == true)
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Mana, Count = int.Parse(TreasuryTextBox.Text) });
            if (ChurchCheckBox.IsChecked == true)
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.People, Count = int.Parse(ChurchTextBox.Text) });
            if (ArmyCheckBox.IsChecked == true)
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Army, Count = int.Parse(ArmyTextBox.Text) });
            if (PeopleCheckBox.IsChecked == true)
                changes.Add(new PairOfCharacteristicAndValue { Characteristic = Characteristic.Faith, Count = int.Parse(PeopleTextBox.Text) });
            return changes;
        }

       
    }
}
