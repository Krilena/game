﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GameLib;
using GameLib.Events;
using GameLib.Solutions;
using Newtonsoft.Json;

namespace EventCreator
{
    /// <summary>
    /// Логика взаимодействия для InstantEventCreator.xaml
    /// </summary>
    public partial class InstantEventCreator : Window
    {
        private Dictionary<long, IEvent> _events;
        public InstantEventCreator(Dictionary<long,IEvent> events)
        {
            InitializeComponent();
            createButton.IsEnabled = false;
            _events = events;
            foreach (var item in grid.Children.OfType<TextBox>())
            {
                item.TextChanged += TextBox_TextChanged;
            }
        }        

        private void EnableButton()
        {                      
            createButton.IsEnabled = !grid.Children.OfType<TextBox>().Any(x => string.IsNullOrEmpty(x.Text));
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableButton();
        }

        private void InstantEventValue_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            InstantEvent instantEvent = new InstantEvent
            {
                Solutions = new List<SolutionWithInstantChanges>()
            };           
            var eventFileName = InstantEventFileName.Text;
            var eventRollbackTime = InstantEventRollbackTime.Text;                
            var firstSolution = new SolutionWithInstantChanges();
            var altSolution = new SolutionWithInstantChanges();  

            InstantSolutionCreator instantSolutionCreator = new InstantSolutionCreator(firstSolution, "Создайте первый выбор");
            instantSolutionCreator.ShowDialog();            
            instantSolutionCreator = new InstantSolutionCreator(altSolution, "Создайте альтернативный выбор");
            instantSolutionCreator.ShowDialog();                   
            instantEvent.Solutions.Add(firstSolution);
            instantEvent.Solutions.Add(altSolution);
            instantEvent.Text = InstantEventTextBox.Text;
            instantEvent.FileName = eventFileName;
            instantEvent.TimeToRollback = int.Parse(eventRollbackTime);
            instantEvent.Id = GetNewId();

            _events.Add(instantEvent.Id,instantEvent);

            MessageBox.Show("Событие успешно создано");
            Clear();
            Close();
        }

        private long GetNewId()
        {
            long id;
            do
            {
                id = new Random(DateTime.Now.Millisecond).Next();
            } while (_events.ContainsKey(id));

            return id;
        }

        private void Clear()
        {
            InstantEventFileName.Text = "";
            InstantEventRollbackTime.Text = "";
            InstantEventTextBox.Text = "";
        }
    }
}
