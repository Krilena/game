﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GameLib.Events;
using GameLib.ForSaveAndLoad;

namespace EventCreator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Dictionary<long, IEvent> _events;
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                _events = new AllEventsSaverAndLoader(@"..\..\..\GameWpf\Data\Events\Events.txt").Load();
            }
            catch
            {
                _events = new Dictionary<long, IEvent>();
            }
        }

        private void Create_Event(object sender, RoutedEventArgs e)
        {
            EventCreatorWindow eventCreator = new EventCreatorWindow();
            eventCreator.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            InstantEventCreator instantEventCreator = new InstantEventCreator(_events);
            instantEventCreator.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DeferredEventCreator deferredEventCreator = new DeferredEventCreator(_events);
            deferredEventCreator.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            EndEventCreator endEventCreator = new EndEventCreator(_events);
            endEventCreator.Show();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            new AllEventsSaverAndLoader(@"..\..\..\GameWpf\Data\Events\Events.txt").Save(_events);
        }
    }
}
