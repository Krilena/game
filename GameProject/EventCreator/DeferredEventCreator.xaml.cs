﻿using GameLib;
using GameLib.Events;
using GameLib.Solutions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EventCreator
{
    /// <summary>
    /// Interaction logic for DeferredEventCreator.xaml
    /// </summary>
    public partial class DeferredEventCreator : Window
    {
        private Dictionary<long, IEvent> _events;
        public DeferredEventCreator(Dictionary<long, IEvent> events)
        {
            InitializeComponent();
            _events = events;
            createButton.IsEnabled = false;
            foreach (var item in grid.Children.OfType<TextBox>())
            {
                item.TextChanged += TextBox_TextChanged;
            }
        }

        private void EnableButton()
        {
            createButton.IsEnabled = !grid.Children.OfType<TextBox>().Any(x => string.IsNullOrEmpty(x.Text));
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableButton();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DeferredEvent deferredEvent = new DeferredEvent();
            deferredEvent.Solutions = new List<SolutionWithSingleEvent>();
            var eventFileName = DeferredEventFileName.Text;
            var eventRollbackTime = DeferredEventRollbackTime.Text;
            var firstSolution = new SolutionWithSingleEvent();
            var altSolution = new SolutionWithSingleEvent();

            SingleEventSolutionCreator singleEventSolutionCreator = new SingleEventSolutionCreator(firstSolution, "Создайте первый выбор", _events);
            singleEventSolutionCreator.ShowDialog();
            singleEventSolutionCreator = new SingleEventSolutionCreator(altSolution, "Создайте альтернативный выбор",_events);
            singleEventSolutionCreator.ShowDialog();

            deferredEvent.Solutions.Add(firstSolution);
            deferredEvent.Solutions.Add(altSolution);
            deferredEvent.Text = DeferredEventTextBox.Text;
            deferredEvent.FileName = eventFileName;
            deferredEvent.TimeToRollback = int.Parse(eventRollbackTime);

            deferredEvent.Id = GetNewId();
            _events.Add(deferredEvent.Id, deferredEvent);

            MessageBox.Show("Событие успешно создано");
            Clear();
        }

        private long GetNewId()
        {
            long id;
            do
            {
                id = new Random(DateTime.Now.Millisecond).Next();
            } while (_events.ContainsKey(id));

            return id;
        }

        private void Clear()
        {
            DeferredEventFileName.Text = "";
            DeferredEventRollbackTime.Text = "";
            DeferredEventTextBox.Text = "";
        }
    }
}
